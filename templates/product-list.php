<?php //print_r( $products );

foreach( $products as $pkey => $product ):?>
<div class="product-widget-inner">
	<?php if( !empty( $product->picture_thumbnail ) ):?>
		<img src="<?php echo $product->picture_thumbnail;?>">
	<?php endif;?>
		<div>
			<h3><?php echo $product->post_title;?></h3>
		</div>
		<div class="star-rating">			
			<?php for( $r = 0; $r < $product->rating; $r++ ){?>
				<span class="fa fa-star"></span>
			<?php }?>
		</div>	
</div>
<?php endforeach;