<?php
namespace Ratings;
/**
 *  Product rating widget
 * 
 * 
 */
if (!defined('ABSPATH')) exit;
class ProductRatingWidget extends \WP_Widget
{
	private $widgetform;
	private $productRating;
	private $remembered;
	private $transientKey;
	function __construct()
	{
		parent::__construct( 'product_rating_widget', 
							__('Product List', 'product_rating_widget_domain'), 
							array( 'description' => __( 'Product Rating Widget', 'product_rating_widget_domain' ), 
						) );
		$this->productRating = new ProductRating;

		if( isset(  $_COOKIE[REMEMBERED_COOKIE_KEY] ) ){
			$this->transientKey = $_COOKIE[REMEMBERED_COOKIE_KEY];
			$this->remembered = get_transient( $this->transientKey );
		}else{
			$this->transientKey = $this->productRating->setProductRatingCookie();
			$this->remembered = null;
		}
	}

	/*
	* Widget
	* Displaying “Products connected to the current Target Group”
	* Remembering the target group
	*/

	public function widget( $args, $instance )
	{		
		
		if( isset( $this->productRating->requests['target'] ) && 
			!empty( $this->productRating->requests['target'] ) && 
			apply_filters( 'group_target_exist', $this->productRating->requests['target'] ) ){

			set_transient( $this->transientKey, $this->productRating->requests['target'], .5 * HOUR_IN_SECONDS );
			$products = $this->productRating->getProducts( null, sanitize_text_field( $this->productRating->requests['target'] ) );

		}else{

			if( !is_null( $this->transientKey ) && !is_null( $this->remembered ) && !empty( $this->remembered ) ){				
				$targetGroup = $this->remembered;				
			}elseif( $this->productRating->defaultTargetGroup ){
				$targetGroupx = get_term( $this->productRating->defaultTargetGroup, 'target_groups' );
				$targetGroup = $targetGroupx->slug;			
			}else{
				$targetGroup = null;
			}

			$products = $this->productRating->getProducts( null, $targetGroup );
		}	
		
		
		echo '<div class="product-widget-container">';
			require_once( PLUGIN_DIR_PATH.'/templates/product-list.php' );
		echo '</div>';
	}

	public function update( $new_instance, $old_instance )
	{
		$instance = array();
		$instance = $old_instance;	

		return $instance;
	}

	public function form( $instance )
	{
		$products = $this->productRating->getProducts();		
		?>
			<div class="product-rating-admin-widget">	
				<div class="product-rating-widget-form">
					<?php if( count( (array) $products ) > 0 ):?>
						<span>Display Products with ratings</span>
					<?php else:?>
						<span><?php _e('No product/s available');?></span>
					<?php endif;?>
				</div>
			</div>
		<?php
	}
	
}