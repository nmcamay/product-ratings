<?php
namespace Ratings;
/**
 * 
 */
if (!defined('ABSPATH')) exit;
class ProductRating
{	
	public $requests;
	private $productStars;
	public $defaultTargetGroup;
	
	function __construct()
	{
		$this->requests = $_REQUEST;
		$this->productStars = 0;
		$this->defaultTargetGroup = absint( get_option('_default_target_group') );		
	}

	/*
	 *  registering product post type
	 */
	public function createProductPostType()
	{
		// registering product post type
		wp_enqueue_style('mtcommerce-micros', PLUGIN_DIR_URL.'assets/css/exam-sparks.css', false, '1.0', 'all');
		wp_enqueue_style('font-awesome', 
			'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', false, '4.7.0', 'all');
		
		register_post_type('product', // Register Custom Post Type
	        array(
	        'labels' => array(
	            'name' => __('All Products'), // Rename these to suit
	            'singular_name' => __('Product'),
	            'add_new' => __('Add New Product'),
	            'add_new_item' => __('Add New Product'),
	            'edit' => __('Edit'),
	            'edit_item' => __('Edit Product'),
	            'new_item' => __('New Product'),
	            'view' => __('View Products'),
	            'view_item' => __('View Product'),
	            'search_items' => __('Search Products'),
	            'not_found' => __('No Product(s) found'),
	            'not_found_in_trash' => __('No Products found in Trash'),
	            'featured_image' => __( 'Picture (Media)' ),
				'set_featured_image' => __( 'Set Picture (Media)' ),
				'remove_featured_image' => __( 'Remove Picture (Media)' ),
				'use_featured_image' => __( 'Use as Picture (Media)' )
	        ),
	        'public' => true,
	        'hierarchical' => true,
	        'has_archive' => true,
	        'supports' => array(
				            'title',
				            'editor',
				            'excerpt',
				            'thumbnail'
	        			),
	        'can_export' => true,
	        'taxonomies' => array(
	            'post_tag',
	            'target_groups'
	        )
	    ));

		// registering Target Groups taxonomy
	    $labels = array(
	        'name'              => __('Target Groups'),
	        'singular_name'     => __('Target Group'),
	        'search_items'      => __('Search Target Groups'),
	        'all_items'         => __('All Target Groups'),
	        'parent_item'       => __('Parent Target Group'),
	        'parent_item_colon' => __('Parent Target Group:'),
	        'edit_item'         => __('Edit Target Group'), 
	        'update_item'       => __('Update Target Groups'),
	        'add_new_item'      => __('Add New Target Group'),
	        'new_item_name'     => __('New Product Target Group Name'),
	        'menu_name'			=> __('Target Groups'),
	    );
	    
	    $args = array(
	        'hierarchical'      => true,
	        'labels'            => $labels,			    
	        'show_admin_column' => true,
	        'query_var'         => true,
	        'rewrite'           => array( 'slug' => '/target' ),
	        'sort' 				=> true,
	        'show_in_nav_menus'	=> true,
	        'show_ui'           => true,
	        'show_in_menu'		=> true,
	    );
	    register_taxonomy( 'target_groups','product', $args );
	}

	/*
	 *  adding metabox for Product Rating/Star
	 */
	public function productStarsField()
	{
		add_meta_box( 'product_stars', 
					__('Stars'), 
					function(){
						return self::productStarCallback();						
					}, 'product', 'side', 'high', null );
	}

	/*
	 *  displaying Product Rating/Star dropdown
	 */
	static public function productStarCallback()
	{
		global $post;
		$metaboxHtml = '';
		$productStars = 0;
		$productStars = absint( get_post_meta( $post->ID, '_product_stars', true ) );
		
		$metaboxHtml .= '<select name="product_meta[product_stars]" id="product-stars" style="height: 45px;width: 50%;">';
			for( $i = 1; $i <= 5; $i++ ){
				$selected = ( $productStars === $i ) ? ' selected="selected"' : '';				
					$metaboxHtml .= '<option value="'.$i.'"'.$selected.'>';
					$metaboxHtml .= $i.'</option>';
			}
		$metaboxHtml .= '</select>';

		_e( $metaboxHtml );
	}

	/*
	* Setting for “Default Target Group”
	*
	*/

	public function productRateOption()
	{
		add_menu_page( __(''), __('Default Target Groups'), 'manage_options', 'defaultTargetGroupCallBack',  function(){ return self::defaultTargetGroupCallBack(); }, 'dashicons-store', 59 );
		
	}

	public function defaultTargetGroupCallBack()
	{
		$optionHtml = "";	
		$targetGroups = $this->getAllTargetGroups();
		if( isset( $this->requests['options']['default_target_group'] ) &&
		 	!empty( $this->requests['options']['default_target_group']) ){
			$this->defaultTargetGroup = sanitize_text_field( $this->requests['options']['default_target_group'] );
			update_option( '_default_target_group', $this->defaultTargetGroup );
		}
		$defTargetGroup = absint( get_option('_default_target_group', 0 ) );		
		$optionHtml .= '<div class="plugin-admin-option">';
			if( count( $targetGroups ) > 0 ){
				$optionHtml .= '<form method="POST" name="frmDefaultTargetGroup" style="margin-top:5%;">';
					$optionHtml .= '<select name="options[default_target_group]" style="height: 45px;width: 30%;">';
							$optionHtml .= '<option value="0"> Select default Target Group </option>';
						foreach( $targetGroups as $tkey => $targetGroup ){
							$selected = ( $defTargetGroup === $targetGroup->term_id ) ? 'selected="selected"' : '';

							$optionHtml .= '<option value="'.$targetGroup->term_id.'"';
							$optionHtml .= $selected .'>';
								$optionHtml .= $targetGroup->name;
							$optionHtml .= '</option>';
						}
					$optionHtml .= '</select>';
					$optionHtml .= '<br><button name="save_option" class="button button-primary button-large" style="margin-top:20px;">Save Option</button>';
				$optionHtml .= '</form>';
			}else{
				$optionHtml .= '<span>No Target Groups</span>';
			}
		$optionHtml .= '</div>';

		_e( $optionHtml );
	}	

	public function saveProductMeta()
	{
		global $post;
		if( isset( $post ) && $post->post_type === 'product' ){

			if( isset( $this->requests['product_meta'] ) ){
				foreach( $this->requests['product_meta'] as $key => $product_meta ){
					delete_post_meta( $post->ID, '_'.$key ); // resetting stars value in preparation for new value
					update_post_meta( $post->ID, '_'.$key, $product_meta );
				}
			}
		}
	}
	/*
	 *  retrieving products together with their stars and picture
	 *	params $args = array; $targetGroup = null
	 */
	public function getProducts( $args = array(), $targetGroup = null )
	{
		$productsRaw = null;
		$products = null;
		$args['post_type'] = 'product';
		if( $targetGroup ){
			
			$args['tax_query'] = array( array( 'taxonomy' => 'target_groups', 
										'field' => 'slug',
										'terms' => $targetGroup,											   
										)
									);
		}else{				
			$productsRaw = get_posts( array( $args ) );
		}	

		$args['meta_query'] = array( 'order_clause' => array( 'key' => '_product_stars', 'type' => 'NUMERIC' ) );
		$args['orderby'] = 'order_clause';		

		$productsRaw = get_posts( $args );		
		foreach( $productsRaw as $key => $raw ){
			$products[$key] = $raw;
			$products[$key]->rating = absint( get_post_meta( $raw->ID, '_product_stars', true ) );
			$products[$key]->picture = get_the_post_thumbnail_url( $raw->ID, 'full' );
			$products[$key]->picture_thumbnail =get_the_post_thumbnail_url( $raw->ID, 'thumbnail' );
		}		
		return $products;		
	}

	public function getAllTargetGroups( $hideEmpty = false )
	{
		return get_terms( 'target_groups', array( 'hide_empty' => $hideEmpty ) );
	}
	/*
	* registering Product Rating Widget
	*/
	public function registerProductRatingWidget()
	{
		register_widget( 'Ratings\ProductRatingWidget' );
	}

	public function targetGroupExist( $targetGroupSlug )
	{		
		if( term_exists( $targetGroupSlug, 'target_groups' ) ){
			$term = get_term_by( 'slug', $targetGroupSlug, 'target_groups' );
			return true;
		}else{
			return false;
		}
	}

	/*
	 * Bonus:
	 */
	//public function transientAndCookies()	
	public function setProductRatingCookie( $key = null ){
		if( is_null( $key ) ){
			$key = REMEMBERED_COOKIE_KEY;
		}else{
			$key = $key;
		}
		if( isset( $_COOKIE[$key] ) && !empty( $_COOKIE[$key] ) ){
			return $_COOKIE[$key];
		}else{
			if( !empty( $key ) ){
				$cookie_string = base64_encode( rand().time() );
				setcookie( $key, $cookie_string, time()+2000 ,'/');
				if( isset( $_COOKIE[$key] ) ){

					return $_COOKIE[$key];	
				}else{

					return $cookie_string;
				}
			}
		}
	}
}