<?php
/*
Plugin Name: Product Ratings
Description: The project covers the ability to display Product ratings on a Wordpress powered site ( Pretest ).
Version: 1.0
Author: Noel M. Camay
Author URI: ''
License: GPLv2 or later
Text Domain: product-ratings
*/
define('PLUGIN_DIR_URL', plugin_dir_url( __FILE__ ) );
define('PLUGIN_DIR_PATH', plugin_dir_path( __FILE__ ) );
define('REMEMBERED_COOKIE_KEY', 'rememberedKey');

require( PLUGIN_DIR_PATH."vendor/autoload.php" );


use Ratings\ProductRating;

$rating = new Ratings\ProductRating;
/*------------------------------------*\
				Actions
\*------------------------------------*/
add_action( 'init', array( $rating, 'createProductPostType' ) );

add_action(	'add_meta_boxes', array( $rating, 'productStarsField' ) );
add_action( 'widgets_init', array( $rating, 'registerProductRatingWidget' ) );
add_action( 'save_post', array( $rating, 'saveProductMeta' ) );
add_action( 'admin_menu', array($rating, 'productRateOption') );

/*------------------------------------*\
				Filters
\*------------------------------------*/
add_filter( 'group_target_exist', array( $rating, 'targetGroupExist' ), 10, 1 );
